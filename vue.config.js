module.exports = {
  lintOnSave: false, // 关闭语法检查
  devServer: {
    publicPath: '/',
    port: '8090',
    proxy: {
      '/dev': {
        // 请求前缀
        // target: 'http://localhost:8888', 
        target: 'http://127.0.0.1:8888', 
        pathRewrite: { '^/dev': '' }, 
        changeOrigin: true // 用于控制请求头中的host值
      }
    }
  }
}
